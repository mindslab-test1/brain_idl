# brain_idl

Brain에서 사용하는 proto를 통합 관리하는 repository입니다. idl = Interface Definition Language.

현재는 Audio에 대해서 시범 적용중이며,
관련 진행 현황은 [Audio 관련 proto 정리 작업](https://pms.maum.ai/confluence/pages/viewpage.action?pageId=27941269) 글을 참조해주세요.


## 담당자

Brain 박승원 수석연구원
